<div id="node-<?php print $node->nid; ?>" class="post<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> <?php print $zebra ?>">
  <div class="post_left">
    <div class="date">
      <span class="post-month"><?php print date('M', $node->created); ?></span>
      <span class="post-day"><?php print date('d', $node->created); ?></span>
    </div>
		<?php foreach (taxonomy_node_get_terms($node->nid) as $term) { ?>
      <div class="bookmark">
        <?php if ($image = taxonomy_image_display($term->tid)) { ?>
          <div class="book_item"><?php print $image; ?></div>
        <?php } ?>
      </div>
    <?php } ?>
	</div>
	<div class="post_right">
		<div class="post_header">
			<?php if ($taxonomy) { ?>
			  <div class="categories"><?php print $terms; ?></div>
			<?php } ?>
			<h2 class="titleh2"><a href="<?php print $node_url; ?>" rel="bookmark" title="Permanent Link to <?php print $title; ?>"><?php print $title; ?></a></h2>	
		</div>
		<div class="post_content">
			<div class="post_entry">
			  <?php print $picture; ?>
				<?php print $content; ?>
				<div class="post_bottom">
				  <strong><?php print t('Author'); ?>:</strong> <?php print $name; ?>
				  <?php if ($links) { ?>
				    <div class="comments"><?php print $links; ?></div>
				  <?php } ?>
				</div>
			  <div class="clear-block clear"></div>
	    </div>
		</div>
	</div>		
</div>