<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>">
	<head profile="http://gmpg.org/xfn/11">
		<title><?php print $head_title ?></title>
		<meta name="generator" content="Drupal" />
		<?php print $head; ?>
		<?php print $styles; ?>
		<?php print $scripts; ?>
		<script type='text/javascript' src='<?php print base_path() . drupal_get_path('theme', 'sodelicious'); ?>/js/jquery.innerfade.js'></script>
		<script type='text/javascript' src='<?php print base_path() . drupal_get_path('theme', 'sodelicious'); ?>/js/js_liveclock.js'></script>
		<script type="text/javascript">$(document).ready(function(){ $('.innerFade').innerfade({speed: 'slow', timeout: 5000}); })</script> 
	</head>
	<body class="<?php print $body_classes; ?>">
		<div id="page">
			<div id="header">
				<div id="header_title">
					<h1 class="titleh1"><a href="<?php print base_path(); ?>"><?php print $site_name; ?></a></h1>
					<div class="description"><?php print $site_slogan; ?></div>
				</div>
				<div id="slideshow__">
					<div id="slideshow_">
						<div id="slideshow">
							<?php print $header; ?>
						</div>
					</div>
				</div>
				<div id="menu">
					<div class="menu_right"></div>
					<?php if (isset($primary_links)) {
  						print theme('links', array_reverse($primary_links), array('id' => 'menu_items', 'class' => 'page_item'));
					} ?>
					<div class="menu_left"></div>
				</div>
				<div id="introduction">
					<div class="introduction_text">
						<?php print theme_get_setting('mission', false); ?>
					</div>			
				</div>
				<div id="side_header">
					<div class="side_header_content">
						<div id="clock_">
		  				<div id="clock_img"></div>
							<div id="clock"><script type="text/javascript">showdate();</script></div>
						</div>
						<div class="side_header_item">
							<?php if ($sponsors) { print $sponsors; } ?>
							<p style="float:right; padding-right:10px;"><a href="/node/feed"><img src="<?php print base_path() . drupal_get_path('theme', 'sodelicious'); ?>/images/feed_rss.gif" alt="Feed Rss" border="0" /></a></p>
						</div>
						<div id="search_box"><?php print $search_box; ?></div>
					</div>
				</div>
			</div>
			<div id="page_">
				<div id="page_content">
				<div id="side">
					<div id="sidebar_">
						<div id="sidebar__">
							<div id="sidebar1">
								<?php print $left; ?>
							</div>
							<div id="sidebar2">
								<?php print $right; ?>
							</div>
							<div id="sidebar3">
								<?php print $bottom; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="post-main">
					<?php if ($breadcrumb) { print $breadcrumb; } ?>
					<?php if ($tabs) { print '<div id="tabs-wrapper" class="clear-block">'; } ?>
					<?php if ($title) { print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; } ?>
					<?php if ($tabs) { print $tabs .'</div>'; } ?>
					<?php if (isset($tabs2)) { print $tabs2; } ?>
					<?php if ($help) { print $help; } ?>
					<?php if ($show_messages && $messages) { print $messages; } ?>
				</div>
				<div id="the_content">
	  			<?php print $content; ?>
				</div>
			</div>
			<div class="union"></div>	
		</div>
		  <div id="footer_">
			  <div id="footer">
      	  <?php if ($footer) { print $footer; } ?>
      	  <?php if ($closure) { print $closure; } ?>
			  </div>
			  <div class="union2"></div>
			  <div id="footer_credits">
				  <div class="footer_credits_txt">Theme design by <a href="http://www.web2themes.com" title="Wordpress Themes" target="_blank">Web 2.0 Themes</a>. Supported by <a href="http://www.bid4traffic.com/phplinkbidtemplates.html" target="_blank">Free phplinkbid templates</a>, <a href="http://www.text-link-easy.com/" target="_blank">Bid directory</a> and <a href="http://www.topgreencars.com/" target="_blank">Green cars info.</a></div>
			  </div>
		  </div>	
	  </div>
  </body>
</html>