<div id="comment-<?php print $comment->cid; ?>" class="comment<?php print ($comment->new) ? ' comment-new' : ''; print (isset($comment->status) && $comment->status  == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">
  <div class="post_left">
    <div class="date">
      <span class="post-month"><?php print date('M', $comment->timestamp); ?></span>
      <span class="post-day"><?php print date('d', $comment->timestamp); ?></span>
    </div>
  </div>
  <div class="post_right">
    <div class="post_header">
      <h3 class="titleh3"><?php print $title; ?></h3>
      <?php if ($comment->new) { ?>
        <a id="new"></a><span class="new"><?php print drupal_ucfirst($new); ?></span>
      <?php } ?>
    </div>
    <?php if ($links) { ?>
      <div class="comment_icon"></div>
      <div class="post-tags"><?php print $links; ?></div>
    <?php } ?>
    <div class="post_content">
      <?php print $picture ?>
      <?php print $content; ?>
      <div class="post_bottom">
        <?php print t('by') . ' ' . $comment->name; ?>
      </div>
      <div class="clear-block clear"></div>
    </div>
  </div>
</div>